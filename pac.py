from tkinter import *
from tkinter import ttk



# On cree premiere fenetre
root = Tk()



# On personnalise la fenetre principal
root.title("Pac-man")
root.geometry("500x500")
root.resizable(height=False,width=False)
root.iconphoto(True, PhotoImage(file='logo.gif'))
root.config(background='#1D1D21')



# ajout d'une image comme fond d'écran
fondscreen = PhotoImage(file='bg.png')
label = Label(root, image=fondscreen)
label.place(x='13', y='95')


# ajout texte et de sa personnalisation
label_title = Label(root, text="Bienvenue sur Pac-man !", font=("Courrier", 23), bg='#1D1D21', fg="white")
label_title.pack()


label_subtitle = Label(root, text="Ceci est mon projet tkinter ", font=("Courrier", 18), bg='#1D1D21', fg="white")
label_subtitle.pack()




# création de fenetre secondaire
def start_window():
  extra = Toplevel(root)
  extra.geometry("500x450")
  extra.resizable(height=False, width=False)
  label1 = Label(extra,width=20,height=10,text="Ce sera la fenetre du jeu")
  label1.pack()




def option_window():
  extra = Toplevel(root)
  extra.geometry("250x250")
  extra.resizable(height=False, width=False)
  label2 = Label(extra,width=27,height=10,text="Choisi la difficulté")

# on ajoute une liste déroulante pour la difficluté du jeu
  comboExample = ttk.Combobox(extra, values=[
                                              "Facile",
                                              "Normal",
                                             "Difficile"])
  comboExample.pack()
  comboExample.current(1)
  label2.pack()




# ajout de bouttons
start_button = Button(text="Commencer une partie", font=("Arial", 20), bg='#DCDCEE', fg="black", command=start_window)
start_button.place(x='100', y='275')


option_button = Button(text="Configuration du jeu", font=("Arial", 20), bg='#DCDCEE', fg="black", command=option_window)
option_button.place(x='115', y='345')


exit_button = Button(text="Quitter le jeu", font=("Arial", 20), bg='#DCDCEE', fg="black", command=root.destroy)
exit_button.place(x='155', y='415')






# permet d'afficher la fenetre
root.mainloop()
